// Import next module
import Image from 'next/image'
import router, { useRouter } from 'next/router'

// Import dummy data
import contacts from '../data/dummy'

export default function Home() {
  return (
    <div className="mx-auto py-6 px-4">
      <section id="header">
        <h1 className="font-semibold text-4xl">Contacts</h1>
        <div className="bg-[#F4F5F7] flex mt-5 px-6 py-3 rounded-[20px] w-full">
          <svg className="fill-current text-[#9B9B9B]" xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 24 24">
            <path d="M10 18a7.952 7.952 0 0 0 4.897-1.688l4.396 4.396 1.414-1.414-4.396-4.396A7.952 7.952 0 0 0 18 10c0-4.411-3.589-8-8-8s-8 3.589-8 8 3.589 8 8 8zm0-14c3.309 0 6 2.691 6 6s-2.691 6-6 6-6-2.691-6-6 2.691-6 6-6z"></path>
          </svg>
          <input className="bg-transparent outline-none px-4 w-full" placeholder="Search name ..." type="text" />
        </div>
      </section>
      <section className="my-5" id="favourite">
        <h1 className="font-semibold mb-[23px] text-xl">Favourite</h1>
        <div className="flex overflow-x-scroll">
          {contacts.map((contact, index) =>
            contact.isFav ? (
              <div className="flex flex-col items-center justify-center mr-7 last:mr-0 min-w-[60px]" key={index} onClick={() => router.push(`/contact/${contact.name}`)}>
                <Image src={contact.img} width={60} height={60} alt={contact.name} />
                <p className="truncate w-[50px]">{contact.name}</p>
              </div>
            ) : (
              ''
            )
          )}
        </div>
      </section>
      <section id="contact">
        <h1 className="font-semibold mb-[23px] text-xl">My Contact ({contacts.length})</h1>
        {contacts.map((contact, index) => (
          <div className="flex items-center mb-5 last:mb-0" key={index} onClick={() => router.push(`/contact/${contact.name}`)}>
            <Image src={contact.img} height={60} width={60} alt={contact.name} />
            <p className="font-medium ml-5 opacity-75 text-lg truncate">{contact.name}</p>
          </div>
        ))}
        <button className="bg-gradient-to-r from-[#02A4FF] to-[#2400FF] p-5 fixed bottom-[5%] right-[16px] rounded-full" name="add-contact">
          <svg className="fill-current text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40">
            <path d="M19 8h-2v3h-3v2h3v3h2v-3h3v-2h-3zM4 8a3.91 3.91 0 0 0 4 4 3.91 3.91 0 0 0 4-4 3.91 3.91 0 0 0-4-4 3.91 3.91 0 0 0-4 4zm6 0a1.91 1.91 0 0 1-2 2 1.91 1.91 0 0 1-2-2 1.91 1.91 0 0 1 2-2 1.91 1.91 0 0 1 2 2zM4 18a3 3 0 0 1 3-3h2a3 3 0 0 1 3 3v1h2v-1a5 5 0 0 0-5-5H7a5 5 0 0 0-5 5v1h2z"></path>
          </svg>
        </button>
      </section>
    </div>
  )
}
