const contacts = [
  { name: 'Ayu Dewi', isFav: true, img: '/img/contact (1).png', wa: 088299001343, email: 'ayud@gmail.com', telegram: '@ayud', ig: '@ayud' },
  { name: 'Bayu Darmawan', isFav: false, img: '/img/contact (2).png', wa: 088299001343, email: 'byu@gmail.com', telegram: '@byu', ig: '@byu' },
  { name: 'Cecilia Wati', isFav: false, img: '/img/contact (3).png', wa: 088299001343, email: 'ccwat@gmail.com', telegram: '@ccwat', ig: '@ccwat' },
  { name: 'Doni Lutfian', isFav: false, img: '/img/contact (4).png', wa: 088299001343, email: 'donil@gmail.com', telegram: '@donil', ig: '@donil' },
  { name: 'Erwin Smith', isFav: true, img: '/img/contact (5).png', wa: 088299001343, email: 'erws@gmail.com', telegram: '@erws', ig: '@erws' },
  { name: 'Farah Fatimah', isFav: true, img: '/img/contact (6).png', wa: 088299001343, email: 'frtimah@gmail.com', telegram: '@frtimah', ig: '@frtimah' },
  { name: 'Galih Yudian', isFav: true, img: '/img/contact (7).png', wa: 088299001343, email: 'gayudian@gmail.com', telegram: '@gayudian', ig: '@gayudian' },
  { name: 'Hilmi', isFav: false, img: '/img/contact (8).png', wa: 088299001343, email: 'himi@gmail.com', telegram: '@himi', ig: '@himi' },
  { name: 'Iwan Subagyo', isFav: true, img: '/img/contact (9).png', wa: 088299001343, email: 'iwanyo@gmail.com', telegram: '@iwanyo', ig: '@iwanyo' },
  { name: 'Jihan Julian', isFav: false, img: '/img/contact (10).png', wa: 088299001343, email: 'jjian@gmail.com', telegram: '@jjian', ig: '@jjian' },
]

module.exports = contacts
